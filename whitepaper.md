<h1>Calibrae Whitepaper</h1>

![Logo](/src/assets/calibrae-logo-256.png)

<em>January 2018 Loki Verloren</em>

<h3>Next Generation Peer to Peer Social Network system</h3>

Calibrae is a radical new design for a wholly monetised peer to peer network system that crowdsources security and infrastructure provision and is designed to be a platform that will grow in its complexity to include many distinct types of services including specialist applications, potentially low latency realtime applications, providing every type of communication method available, into a single, open and modular platform.

<h2>Table of Contents</h2>

<!-- TOC -->

* [0. Introduction](#0-introduction)
* [1. Overview](#1-overview)
  * [1.0. Equitable Distribution Systems](#10-equitable-distribution-systems)
  * [1.1. Reputation Mechanics](#11-reputation-mechanics)
  * [1.2. Peer To Peer Monetised Service](#12-peer-to-peer-monetised-service)
  * [1.3. Modular Peer to Peer Network for Resilience and Security](#13-modular-peer-to-peer-network-for-resilience-and-security)
  * [1.4. Protocol Differentiated Tokens and Internal Market](#14-protocol-differentiated-tokens-and-internal-market)
  * [1.5. Mycelium Based Data Replication and Authentication](#15-mycelium-based-data-replication-and-authentication)
  * [1.6. Multiple Data Models for Optimal Resource Utilisation](#16-multiple-data-models-for-optimal-resource-utilisation)

<!-- /TOC -->

## 0. Introduction

In the field of cryptocurrency up until now, there has been very little consideration for matters relating to social order and governance. Many cryptocurrency distribution patterns are highly centralised or tend towards aggregation because of the effect of compounding interest.

The most extreme form is the pyramid scheme, and you can walk down a continuum and pick a spot where most of cryptocurrencies are. In actual fact, central banks exactly do this with their business with the various banking corporations they deal with, and so on down until the average employed person and sole trader are both on the receiving end of the 'last money', after it has finally and very slowly trickled down.

So, in order to solve this problem, Calibrae is designed to provide an earning opportunity for anyone, from journalism, computer programming, to retail, to infrastructure provision and financial market activities.

A key goal of Calibrae is in further enhancing the resilience of the network against attacks by large actors or catastrophe causing partitioning of the network. Thus it will have location obfuscation and indeed programmable transport so that it will live on multiple network systems and favour them for applications that run satisfactorily with that protocol. This will include location obfuscation and hidden services, as well as new, experimental scatter path distribution for lower latency.

## 1. Overview

### 1.0. Equitable Distribution Systems

In a financial system, it tends to be that you get a tight centralisation of the holding of a large surplus of currency, or equally, an exclusive or dominant right of issuance.

Ongoing issuance is found in every monetary system, there is a little more gold dug up every year. This issuance rate forms part of the basis of control of interest rates. Or more precisely, it controls attempts to control interest rates. The general character of the distribution is more important than the issuance rate, up to a certain point.

So, in order to provide a more equitable distribution, the right and capacity of someone in the network to issue new tokens to someone, is isolated from the quantity of holdings. As well as this, to keep an equilibrium of distribution, it is possible for a large group to limit another user's power, in a manner similar to vigilante justice. However, such groups can become more organised as well.

For very good reason, it is considered by most that having the right to mint currency for yourself is a bad thing. With Calibrae's reputation system, attempts to use sock puppets to conceal self-mining could be stopped if people knew and agreed also that this is bad behaviour. Antisocial, in fact.

Outright antisocial behaviour like scamming, trolling, and other types of online social network mischief can also be policed. Some think this might lead to endless combat but in fact it's designed to highlight the cost of disharmony in social matters. Both sides in a war against each other will drive their reputations downward until the whole thing becomes patently absurd. Meanwhile, those not engaging in such stupidities will also take up the share of the pool of new tokens that these fighters lose in their nonsense.

As well as this security mechanic, the issuance of new tokens can be earned by doing work for the network, such as running the servers, serving the application, writing code for the network, or indeed ultimately an ecosystem of developers, and eventually specialised applications built within the same protocol, interfaced to the user through the application.

Calibrae is designed so that it can easily be extended to cover new services, and integrate these more cleanly than is possible with centralised web technologies.

### 1.1. Reputation Mechanics

Reputations are raised in accordance with the reputation of the voter at the time of a vote, in proportion with the scale of the reputation as a percentage of the total reputation pool. Likewise downvotes reduce it the same way.

When a user blocks another user, this takes a proportional amount off the effective total, acting like a hold. The proportion is calculated upon the percentage of the user, and then multiplied by a decaying coefficient that drops by a consistent percentage the more users do it. This effect is stronger for the one to one, but to prevent it becoming an offensive weapon, its effect is not in proportion with the plain number of mutes but rather calibrated so that each subsequent user has less effect. The right curve to use will be discovered through the testing phase.

To further prevent it being gamed, it has a 1 day cool-off period before it will deactivate as requested. It also has an increasing cost in a reverse pattern to the compounding effect, such that the greater number of mutes an account asserts, the more these mutes put a hold on the user's account also.

The reputation modulation effects upon accounts with these holds applied counts in proportion with the momentary effective reputation score, not the unmodified base value. Thus the effects of bad behaviour are compounded as it continues.

Further, like with the mute creating an asymmetric hold between the accounts, downvotes also proportionally subtract from the downvoter's reputation based on a moving average such that the more frequently the user downvotes, the more their own reputation is reduced. This also must be calibrated in testing to not interfere with engagement. To balance this, upvotes also take a proportion out, which will disincentivise spamming votes, in either direction.

In effect, the power to issue tokens will be consumed and redistributed the more you use it. This will also drive engagement because as inactivity causes a lack of growth, the inactive user will proportionally lose their right to affect it according to the proportion to which they lie outside the 2 standard deviations of global engagement rate (posting activity). In this respect the model works similarly to Steem's vote power calculation system, but instead of multiplying it by stake, it grows in supply at a fixed percentage rate, to ensure that the granuality of power refines to accommodate a larger userbase.

### 1.2. Peer To Peer Monetised Service

In Calibrae, everyone can run some form of network infrastructure, from simply proxying, to serving up a shard of the several database systems (ledger, marketplace, exchange, application provision), to eventually even custom applications such as for example a semi-decentralised exchange interacting with the interbank payment networks, a game with latency minimisation for precise realtime gaming.

Anyone who runs the database, will also serve up the application and interpose to assign their provider share of the rewards given to posts on the network through their node. This eliminates the need for an election to regulate witnesses. Instead their reputation and capacity can dictate how much of a share of the application service payment share they may take. This will be fixed in the denomination of the base currency, as a percentage of total issued.

The individual application segments, the software repository, the forum, data storage hosting, marketplace, and so on, will also have their own token each, in which the rewards are paid out to users after the votes are totaled and rewards assigned. The reason for this is that if multiple reward system platforms are set to fixed percentages, this rigidity can limit flexibility of the system and potentially cause supply problems for users.

Thus, instead, there is a token for each unit of the system, even original posts are distinguished from comments, and by this the supply of each type of content format will adapt to the demand for the token for the format. In this way a person can speculate upon the growth of one format at a given time, as well as earn for helping discover its proportional value against the whole ecosystem.

It essentially ensures that all formats are incentivised exactly according to the interest of users, which is what puts load on and incurs costs on the network.

### 1.3. Modular Peer to Peer Network for Resilience and Security

The peer to peer infrastructure that is necessary for running decentralised networks in many cryptocurrencies is quite primitive. In some cases it is literally a manually edited list, much like the `/etc/hosts` file on a computer with TCP/IP networking.

There will be a shared and certified list of all nodes on the network and associated username distributed to all nodes. The software will ship with a snapshot of the current list, both the user front-end and the server backend software, both via browser application and in a native, electron or other UI framework database. This list functions as an advertisment of available services.

Next, to secure and develop a trust model for this, users can rely both on the various forum reputations of the users operating the services, as well as manually favour them. This will help ensure that the trust given to a node operator is proportional with their standing in the community, as well as their capacity. This will ensure that operators maintain adequate engagement in order to maintain their level of utilisation.

The network system itself will be completely modularised. It will be possible to plug in various transports, such as IPv4, IPv6, Zerotier, Tor, I2P and others, and depending on the latency characteristics and bandwidth capacity of links, will be able to resist being shut down if one network is attacked but others remain operational.

Furthermore, all nodes will have the option to provide proxy service. These proxy services are monetised through the use of a system of prepaid vouchers, traded between nodes. These vouchers will be multi-sig, and ZKP secured certifications of their authenticity without revealing any further details but the routing accounts, and from these proof of service certificates, after accumulation, can be redeemed as currency out of the issuance pool segregated for this service.

In other words, it will have a location security system akin to Tor and I2P built in, to prevent gathering intelligence to attack users via network systems. This will of course be optional. The routing methodologies will be extensible also. Rerouting strategies can be designed and implemented purely on the side of the client, including onion routes and split scatter routing and whatever other designs people might think of.

The same monetisation system will also apply to both storage and compute services.

### 1.4. Protocol Differentiated Tokens and Internal Market

By making a secondary, liquid token for each different format of communication system, it becomes possible for the network to adapt to supply and demand through the revaluation of their own specific token. Thus when the earnings are spent, their value is based on the internal market value determined in an internal exchange market, similar to Bitshares but locked to only apply within a specific format of forum.

A problem for building a multi-format platform with monetisation for all formats, is that the relative value of each type of service varies over time. The more formats you add, the more people start to disagree with each other about who should get what amount out of the pie. Instead of letting it degrade into a pitched battle, you build a marketplace that allows the representations of the economy within each format, compared to each other, and the incentive to put money where growth is imminent, and redirect resources out of formats that are in a slump.

Thus, infrastructure providers will have a token. Forum posters will have a token. Commenters will have a token. Programmers will have a token. Curators in all forums will have a curation token that complements the worker token. Instead of only allowing these market transactions to manually take place, in the withdrawal interface, it can be configured to take from one or multiple format tokens and directly convert using the current available offers for the exchange, and then directly send the converted tokens to another user. As well as sending the actual tokens directly to another user.

### 1.5. Mycelium Based Data Replication and Authentication

Rather than the cumbersome and ever-growing and duplicated nature of blockchain as a distribution method, Calibrae uses a new distributed database architecture based on SporeDB. Sporedb does not store a distinct historical ledger, though the ability to create one should still be available, for those wishing to use this for auditing.

Instead, the fundamental atom of Calibrae's Spore database is a transaction. These transactions flow around the worker nodes via epidemic transmission patterns, and when launching a new node, instead of replaying a ledger (which could still be optional) multiple trusted nodes can be queried, cross-checked and directly written into the shared data file format, which is the same in transmission as it is in storage, like a unix filesystem.

The network is resistant to 51% attacks because so long as one node has a faithful record, this can eventually become the consensus despite attempts to poison it, the logic of the consensus decision process is such that false transactions are not as robust to the tests applied and the network continues to rebroadcast them that are found to be authentic.

Extending from the base of SporeDB, the fundamental database stored in all nodes, is the node database, and the user database. The information in these two databases is combined in order to allow semi-automated selection of trust levels based on either an instance or a user, based on forum activity, as well as historical, verified performance records, also part of this database, all configurable individually by the user or applied to classes or all of their operating nodes.

Furthermore, this one-to-one style of communication should allow it to scale to very large numbers of nodes, because there is no central chokepoint path for communication. It will mean in short periods the network will not be completely consensual, but always rapidly form consensus no matter the conditions of the network or the nature and volume of traffic.

### 1.6. Multiple Data Models for Optimal Resource Utilisation

Similar to how the network transport system is modular and pluggable, so is the storage and processing backends. It is intended that it be possible to develop a generalised application proof of service system, and then within this, implement distinct consensus mechanics to monitor utilisation and optimise it. Thus, nodes can serve simple text, short format, long format, bulk files with both random and streamed delivery, and each format in how it stores its data is optimised for its purpose. Thus, short messages in 256 byte blocks, long format posts in 512 byte blocks, and files stored in megabyte and gigabyte blocks.

It is a key goal of the operation of Calibrae's infrastructure to make it as realtime and dynamic as possible. Attempts to incorporate large packets of data into blockchains, and worse, into a single uniform database format, have led to serious difficulties for those running blockchain nodes. Long sync times, slow playback, and excessive resource utilisation from unoptimised storage and access protocols.
