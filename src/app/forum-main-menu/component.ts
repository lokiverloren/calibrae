import {
  Component,
  OnInit,
  Input,
  Output,
  HostBinding,
  EventEmitter
} from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';

@Component({
  selector: 'app-forum-main-menu',
  template: `
  <mat-toolbar color="primary" class="mat-elevation-z4" class="headerbar">
        <img src="./assets/calibrae-logo.svg" class="logo" />
        <span class="heading">{{title}} Forum</span>
        <button mat-button i18n="trending|Change view to sort by trending" [matMenuTriggerFor]="sortmenu">
          <mat-icon>trending_up</mat-icon>
          <span fxShow.lt-md="false">Sort</span>
        </button>
        <button mat-button FocusSearch i18n="search|Search Calibrae Database" (click)="onSearchForum()">
        <mat-icon>search</mat-icon>
        <span fxHide.lt-md="true">Search</span>
      </button>
    <span class="midspan">

      <button mat-button i18n="write|Write a new post" fxHide.lt-md="true">
        <mat-icon>create</mat-icon>
        <span fxHide.lt-md="true">Write</span>
      </button>
      <button mat-button i18n="replies|View replies to your posts" >
        <mat-icon>reply_all</mat-icon>
        <span fxHide.lt-sm="true">Replies</span>
      </button>

      <button mat-fab color="primary" class="floatingbutton" i18n="write|Write a new post" fxHide.gt-sm="true">
        <mat-icon>create</mat-icon>
      </button>

      </span>

        <mat-menu #langmenu="matMenu">
          <button mat-menu-item>English</button>
          <button mat-menu-item>Deutsche</button>
          <button mat-menu-item>Nederlands</button>
          <button mat-menu-item>български</button>
          <button mat-menu-item>Русский</button>
        </mat-menu>
        <mat-menu #sortmenu="matMenu">
          <button mat-menu-item i18n="trending|Sort by most active">
            <mat-icon>trending_up</mat-icon>
            Trending
          </button>
          <button mat-menu-item i18n="newest|Sort by newest">
            <mat-icon>cached</mat-icon>
            New
          </button>
          <button mat-menu-item i18n="rewards|Sort by pending rewards">
            <mat-icon>monetization_on</mat-icon>
            Pending Rewards
          </button>
          <button mat-menu-item i18n="favorite|Show new posts from followed users">
            <mat-icon>favorite</mat-icon>
            Followed
          </button>
        </mat-menu>

        <span class="midspan" fxShow.lt-xs="false">
          <button mat-button [matMenuTriggerFor]="langmenu" fxShow.lt-md="false">
            <mat-icon>language</mat-icon>
            <span>EN</span>
          </button>
          <button mat-button (click)="changeTheme()" i18n="daymode|Bright theme">
            <mat-icon>{{themeIconName}}</mat-icon>
            <span fxShow.lt-md="false">{{modeIndicator}}</span>
          </button>
        </span>

        <span class="rightspan">
        <span fxHide.lt-sm="true">
          <button mat-button fxHide.lt-sm="true">
            <mat-icon>chat</mat-icon>
            <span fxHide.lt-lg="true">Chat</span>
          </button>
          <button mat-button i18n="exchange|Access Calibrae Exchange">
            <mat-icon>insert_chart</mat-icon>
            <span fxHide.lt-lg="true">Exchange</span>
          </button>
          <button mat-button i18n="market|Access Calibrae Marketplace">
            <mat-icon>shopping_cart</mat-icon>
            <span fxHide.lt-lg="true">Market</span>
          </button>
          <button mat-button i18n="wallet|Access user wallet">
            <mat-icon>account_balance_wallet</mat-icon>
            <span fxHide.lt-lg="true">Wallet</span>
          </button>
        </span>

          <button mat-button i18n="login|Log into Calibrae" fxShow.gt-md="true" fxHide.lt-sm="true">
            <mat-icon>account_circle</mat-icon>
            <span fxHide.lt-md="true">Login</span>
          </button>
          <button mat-button fxHide.gt-md="true">
            <mat-icon>menu</mat-icon>
          </button>
        </span>

    </mat-toolbar>
    `,
  styles: [
    `
    .heading {
      padding-right: 16px;
    }
    .themeicon {
        color: white;
      }

      .logo {
        margin: 8px; padding: 0px;
      }

      .headerbar {
        position: fixed;
        top: 0px;
        z-index: 5;
        height: 64px;
        width: 100%;
        margin: 0px;
        padding: 0px;
        padding-right: 8px;
      }

      .content {
        margin-top: 64px;
      }

      .searchbar {
        height: 64px;
      }

      .searchbox {
        width: 100%;
        margin-right: auto;
        margin-top: auto;
        margin-bottom: auto;
      }

      .mat-input-infix,
      .mat-form-field-infix,
      .mat-form-field {
        margin: 0px;
        margin-bottom: 4px;
        margin-top: 12px;
        padding: 0px;
        margin-right: auto;
      }

      .modaldim {
        width: 100%;
        height: 100%;
        z-index: 4;
        opacity: 0.7;
        background-color: black;
        position: absolute;
      }

      .rightspan {
        margin: 0px;
        margin-left: auto;
        padding: 0px;
      }

      .midspan {
        padding: 0px;
        margin-left: auto;
        margin-right: 0px;
      }

      .leftspan {
        margin: 0px;
        margin-left: 0px;
        margin-right: auto;
        padding: 0px;
      }

      button {
        padding: 2px;
        margin: 0px;
        min-width: 16px;
      }

      .floatingbutton {
        position: fixed;
        bottom: 48px;
        right: 48px;
      }
    `
  ]
})
export class ForumMainMenuComponent implements OnInit {
  @Input() title: string;
  @Input() forumVisible: boolean;
  @Input() viewMode: string;
  @Output() toggle: EventEmitter<any> = new EventEmitter();
  @Output() searchForum: EventEmitter<any> = new EventEmitter();
  isDarkTheme = false;
  themeIconName = 'wb_sunny';
  themeTooltip = 'Click to toggle to night mode';
  modeIndicator = 'Day Mode';
  constructor() {
    console.log(this.viewMode);
  }

  changeTheme(): void {
    console.log(this.viewMode);
    this.toggle.emit(null);
    this.isDarkTheme = !this.isDarkTheme;
    this.themeIconName = this.isDarkTheme ? 'brightness_3' : 'wb_sunny';
    this.themeTooltip = this.isDarkTheme
      ? 'Click to toggle to day mode'
      : 'Click to toggle to night mode';
    this.modeIndicator = this.isDarkTheme ? 'Night Mode' : 'Day Mode';
  }

  onSearchForum() {
    console.log('Search opened');
    this.searchForum.emit(null);
  }
  ngOnInit() {}
}
