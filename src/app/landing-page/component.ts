import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  template: `
    <p>A paragraph.</p>
    <mat-card>This is a card</mat-card>
    <p>A paragraph.</p>
    <mat-card>This is a card</mat-card>
    <p>A paragraph.</p>
    <mat-card>This is a card</mat-card>
`,
  styles: [
    `
    .rightspan {
      margin-left: auto;
    }

    p {
      margin: 0px;
      padding: 8px;
    }

    .heading {
      padding-left: 8px;
      padding-right: 8px;
    }
  `
  ]
})
export class LandingPageComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
