import { Component, OnInit, AfterViewInit, Input } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  template: `
    <ng-content></ng-content>
`,
  styles: [
    `
  `
  ]
})
export class MainMenuComponent implements OnInit, AfterViewInit {
  @Input() viewMode: string;

  constructor() {}

  ngAfterViewInit() {
    console.log(this.viewMode);
  }

  ngOnInit() {}
}
