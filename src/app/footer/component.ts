import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  template: `
    <mat-toolbar color="accent" class="topbar">
      <img src="./assets/calibrae-logo.svg" />
      <span class="heading">{{copyright}}</span>
      <span class="rightspan">
        <a href="/about" mat-button>
          <mat-icon>info</mat-icon>
          <span fxHide.lt-sm="true">About</span>
        </a>
        <a href="https://gitlab.com/lokiverloren/calibrae" mat-button>
          <mat-icon svgIcon="gitlab"></mat-icon>
          <span fxHide.lt-sm="true">Gitlab</span>
        </a>
      </span>
    </mat-toolbar>
`,
  styles: [
    `
    .rightspan {
      margin-left: auto;
    }

    p {
      margin: 0px;
      padding: 8px;
    }

    .heading {
      padding-left: 8px;
      padding-right: 8px;
    }

    button,
    a {
      padding: 0px;
      padding-left: 4px;
      padding-right: 4px;
      margin: 0px;
      margin-left: 4px;
      margin-right: 4px;
      min-width: 16px;
    }
  `
  ]
})
export class FooterComponent implements OnInit {
  copyright = 'Calibrae © 2017';

  constructor() {}

  ngOnInit() {}
}
