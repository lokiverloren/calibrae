import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent, FocusSearchDirective } from './component';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LandingPageComponent } from './landing-page/component';
import { FooterComponent } from './footer/component';
import { ForumMainMenuComponent } from './forum-main-menu/component';
import { MaterialModule } from './material/module';
import { MainMenuComponent } from './main-menu/component';
import { SearchMainMenuComponent } from './search-main-menu/component';

@NgModule({
  declarations: [
    AppComponent,
    FocusSearchDirective,
    LandingPageComponent,
    FooterComponent,
    ForumMainMenuComponent,
    MainMenuComponent,
    SearchMainMenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
