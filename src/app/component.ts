import { MatIconRegistry } from '@angular/material';
import { OverlayContainer } from '@angular/cdk/overlay';
import { DomSanitizer } from '@angular/platform-browser';
import {
  ViewChildren,
  HostBinding,
  OnInit,
  Component,
  Output,
  EventEmitter,
  Input,
  SimpleChange,
  ElementRef,
  Renderer,
  Directive,
  AfterViewInit
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';

@Directive({
  selector: '[FocusSearch]'
})
export class FocusSearchDirective implements AfterViewInit {
  @Input() focus: boolean;
  constructor(private el: ElementRef, private renderer: Renderer) {
    this.el.nativeElement.focus();
  }
  ngAfterViewInit() {
    this.renderer.invokeElementMethod(this.el.nativeElement, 'focus');
  }
}

@Component({
  selector: 'app-root',
  template: `
    <app-main-menu>
      <app-forum-main-menu [title]="title" (toggle)="changeTheme()"></app-forum-main-menu>
    </app-main-menu>
    <mat-sidenav-container [class.dark-theme]="isDarkTheme">
      <mat-sidenav #mainsidenav opened="true" mode="over" position="end" class="sidebarcontent">
        <h1>sidenav</h1>
      </mat-sidenav>
      <mat-sidenav-content class="content">
          <app-landing-page></app-landing-page>
          <app-landing-page></app-landing-page>
          <app-landing-page></app-landing-page>
          <app-footer></app-footer>
        </mat-sidenav-content>
    </mat-sidenav-container>`,
  styles: [
    `.headerbar {
    position: fixed;
    top:0px;
    z-index:5;
    }

    .content {
      margin-top:64px;
    }
    .sidebarcontent {
      margin-top: 64px;
      top: 0px;
      right: 0px;
      position: fixed;
    }

    .searchbar {
      height: 64px;
      box-shadow: 0 10px 20px rgba(0, 0, 0, 0.16), 0 6px 6px rgba(0, 0, 0, 0.23);
    }

    .searchbox {
      width: 100%;
      margin-right: auto;
      margin-top: auto;
      margin-bottom: auto;
    }

    .mat-input-infix,
    .mat-form-field-infix,
    .mat-form-field {
      margin: 0px;
      margin-bottom: 4px;
      margin-top: 12px;
      padding: 0px;
      margin-right: auto;
    }

    .modaldim {
      width: 100%;
      height: 100%;
      z-index: 4;
      opacity: 0.7;
      background-color: black;
      position: absolute;
    }`
  ],
  viewProviders: [MatIconRegistry]
})
export class AppComponent implements OnInit {
  @HostBinding('class') componentCssClass;
  title = 'Calibrae';
  isDarkTheme: boolean;
  themeIconName = 'wb_sunny';
  themeTooltip = 'Click to toggle to night mode';
  searchTerm = '';
  searchVisible = false;
  searchEntered = '';
  modeIndicator = 'Day Mode';
  private searchFocused = false;
  viewMode = 'forum';

  constructor(
    private renderer: Renderer,
    public overlayContainer: OverlayContainer,
    iconReg: MatIconRegistry,
    sanitizer: DomSanitizer
  ) {
    iconReg.addSvgIcon(
      'gitlab',
      sanitizer.bypassSecurityTrustResourceUrl('../assets/gitlab.svg')
    );
    this.isDarkTheme = false;
    this.overlayContainer.getContainerElement().classList.add('bright-theme');
    this.componentCssClass = 'bright-theme';
    console.log(this.viewMode);
  }

  focusSearch() {
    this.searchFocused = true;
    setTimeout(() => {
      this.searchFocused = false;
    });
  }

  changeTheme(): void {
    this.isDarkTheme = !this.isDarkTheme;
    this.isDarkTheme
      ? this.overlayContainer
          .getContainerElement()
          .classList.add('bright-theme')
      : this.overlayContainer.getContainerElement().classList.add('dark-theme');
    this.componentCssClass = this.isDarkTheme ? 'dark-theme' : 'bright-theme';
    this.themeIconName = this.isDarkTheme ? 'brightness_3' : 'wb_sunny';
    this.themeTooltip = this.isDarkTheme
      ? 'Click to toggle to day mode'
      : 'Click to toggle to night mode';
    this.modeIndicator = this.isDarkTheme ? 'Night Mode' : 'Day Mode';
  }

  onForumSearchEnter(value: string) {
    this.viewMode = 'forum';
    this.searchTerm = '';
    this.searchEntered = value;
  }

  onForumSearchSelect() {
    console.log('user clicked search');
    this.viewMode = 'forum-search';
    console.log('focusing input');
    this.focusSearch();
  }

  ngOnInit() {}
}
