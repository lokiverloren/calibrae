import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-main-menu',
  template: `
    <mat-toolbar color="primary" class="mat-elevation-z4" class="headerbar">
    <app-main-menu>
    </app-main-menu>
    <mat-toolbar-row *ngIf="searchVisible" class="searchbar">
      <img src="../assets/calibrae-logo.svg" />

      <span fxShow="false" fxHide.gt-sm="false" class="heading">
        <button mat-button (click)="onSearchEnter()">
          <mat-icon>search</mat-icon> Search
        </button>
      </span>
      <span fxShow="true" fxHide.gt-sm="true" class="heading">
        <button mat-icon-button (click)="onSearchEnter()">
          <mat-icon>search</mat-icon>
        </button>
      </span>

      <mat-form-field class="searchbox">
        <input FocusSearch matInput [(ngModel)]="searchTerm" (keyup.enter)="onSearchEnter()" class="searchinput" />
        <button mat-icon-button *ngIf="searchTerm" matSuffix mat-icon-button aria-label="Clear" (click)="searchTerm=''">
          <mat-icon>backspace</mat-icon>
        </button>
      </mat-form-field>


      <span fxShow="false" fxHide.gt-sm="false" class="heading">
        <button mat-button>
          <mat-icon>settings</mat-icon> Advanced Search
        </button>
      </span>
      <span fxShow="true" fxHide.gt-sm="true" class="heading">
        <button mat-icon-button>
          <mat-icon>settings</mat-icon>
        </button>
      </span>

      <button mat-icon-button (click)="searchTerm=''; searchVisible=!searchVisible">
        <mat-icon>close</mat-icon>
      </button>

    </mat-toolbar-row>
`,
  styles: [
    `
  .themeicon {
    color: white;
  }

  .rightspan {
    margin-left: auto;
  }

  p {
    margin: 0px;
    padding: 8px;
  }

  .heading {
    padding-left: 8px;
    padding-right: 8px;
  }

  .headerbar {
    position: fixed;
    top: 0px;
    z-index: 5;
    height: 64px;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.18), 0 6px 6px rgba(0, 0, 0, 0.10);
  }

  .content {
    margin-top: 64px;
  }

  .searchbar {
    height: 64px;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.16), 0 6px 6px rgba(0, 0, 0, 0.23);
  }

  .searchbox {
    width: 100%;
    margin-right: auto;
    margin-top: auto;
    margin-bottom: auto;
  }

  .searchinput {}

  .mat-input-infix,
  .mat-form-field-infix,
  .mat-form-field {
    margin: 0px;
    margin-bottom: 4px;
    margin-top: 12px;
    padding: 0px;
    margin-right: auto;
  }

  .modaldim {
    width: 100%;
    height: 100%;
    z-index: 4;
    opacity: 0.7;
    background-color: black;
    position: absolute;
  }
  `
  ]
})
export class SearchMainMenuComponent implements OnInit {
  searchVisible: boolean;
  constructor() {}

  ngOnInit() {}
}
